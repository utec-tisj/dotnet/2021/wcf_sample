﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Persona
/// </summary>
public class Persona
{
    public int Id { get; set; }
    public string Documento { get; set; }
    public string Apellidos { get; set; }
    public string Nombres { get; set; }
    public DateTime FechaNacimiento { get; set; }

    public Persona()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}