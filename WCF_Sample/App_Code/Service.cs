﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
public class Service : IService
{
    public static List<Persona> db { get; set; }

    public string GetData(int value)
	{
		return string.Format("You entered: {0}", value);
	}

	public CompositeType GetDataUsingDataContract(CompositeType composite)
	{
		if (composite == null)
		{
			throw new ArgumentNullException("composite");
		}
		if (composite.BoolValue)
		{
			composite.StringValue += "Suffix";
		}
		return composite;
	}

    Status IService.AddPersona(Persona persona)
    {
        if (db == null)
            db = new List<Persona>();

        db.Add(persona);

        return new Status() { StatusOk = true, StatusMge = "" };
    }

    string IService.GetData(int value)
    {
        throw new NotImplementedException();
    }

    CompositeType IService.GetDataUsingDataContract(CompositeType composite)
    {
        throw new NotImplementedException();
    }

    List<Persona> IService.GetPersonas()
    {
        return db;
    }
}
